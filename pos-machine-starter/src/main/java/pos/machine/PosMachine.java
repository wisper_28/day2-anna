package pos.machine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);
        Receipt receipt = countCost(receiptItems);
        return renderReceipt(receipt);
    }
    public List<ReceiptItem> decodeToItems(List<String> barcodes){
        List<Item> items = loadAllItems();
        List<ReceiptItem> receiptItems = new ArrayList<>();
        Map<String,Integer> quantityMap = new HashMap<>();
        barcodes.forEach(barcode -> {
            quantityMap.put(barcode, quantityMap.getOrDefault(barcode, 0) + 1);
        });
        items.forEach(item->{
            ReceiptItem receiptItem = new ReceiptItem();
            receiptItem.setName(item.getName());
            receiptItem.setUnitPrice(item.getPrice());
            receiptItem.setQuantity(quantityMap.get(item.getBarcode()));
            receiptItems.add(receiptItem);
        });
        return receiptItems;
    }
    public Receipt countCost(List<ReceiptItem> receiptItems){
        Receipt receipt = new Receipt();
        receipt.setReceiptItems(countItemsCost(receiptItems));
        receipt.setTotalPrice(countTotalPrice(receiptItems));
        return receipt;
    }
    public String renderReceipt(Receipt receipt){
        String itemsReceipt = generateItemsReceipt(receipt);
        return generateReceipt(itemsReceipt,receipt.getTotalPrice());
    }
    public List<Item> loadAllItems(){
        return ItemsLoader.loadAllItems();
    }
    public List<ReceiptItem>  countItemsCost(List<ReceiptItem> receiptItems){
        receiptItems.forEach(receiptItem -> {
            receiptItem.setSubTotal(receiptItem.getQuantity()*receiptItem.getUnitPrice());
        });
        return receiptItems;
    }
    public int countTotalPrice(List<ReceiptItem> receiptItems){
        int total = 0;
        for(int i = 0; i < receiptItems.size(); i ++) {
            total += receiptItems.get(i).getSubTotal();
        }
        return total;
    }
    public String generateItemsReceipt(Receipt receipt){
        String itemsReceipt = "***<store earning no money>Receipt***\n";
        List<ReceiptItem> receiptItems=receipt.getReceiptItems();
        for(int i = 0; i < receiptItems.size(); i ++){
            ReceiptItem receiptItem = receiptItems.get(i);
            String itemLine = "Name: " + receiptItem.getName() + ", Quantity: " + receiptItem.getQuantity() + ", Unit price: " + receiptItem.getUnitPrice() + " (yuan), Subtotal: " + receiptItem.getSubTotal() + " (yuan)\n";
            itemsReceipt = itemLine + itemsReceipt;
        }
        return itemsReceipt;
    }
    public String generateReceipt(String itemsReceipt,int totalPrice){
        String receipt = itemsReceipt + "----------------------\n";
        receipt += "Total: " + totalPrice + " (yuan)\n";
        receipt += "**********************";
        return receipt;
    }
}
